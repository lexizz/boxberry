<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Article extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'article';

    protected $fillable = [
        '_id', 'id', 'title'
    ];
}
